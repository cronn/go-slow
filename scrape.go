package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	// [IMPORT MISSING FOR ROUTING]
)

// Models
type User struct {
	Id   int    `json:uid`
	Name string `json:name`
}

// [POLL MODEL HERE]

// Controllers
func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Welcome!\n")
}

// [USER DETAILS HERE]

func PollDetails(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	p := Poll{"Meine Umfrage", "Eine Beschreibung", "philipp@cronn.de"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(p)
}

func VoteForPoll(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprintf(w, "VOTING! %v\n", ps)
}

// Application Init
func main() {
	router := httprouter.New()
	router.GET("/", Index)

	router.GET("/polls", ListPolls)
	router.GET("/polls/:uniqueToken", PollDetails)
	// [USER ROUTE MISSING]
	router.GET("/polls/:uniqueToken/vote/:pollId", VoteForPoll)

	log.Fatal(http.ListenAndServe(":8080", router))
}
